; utct_profile make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[navbar][version] = "1.7"
projects[navbar][subdir] = "contrib"

projects[ctools][version] = "1.9"
projects[ctools][subdir] = "contrib"

projects[utct_misc_configuration_feature][type] = "module"
projects[utct_misc_configuration_feature][subdir] = "features"
projects[utct_misc_configuration_feature][download][type] = "git"
projects[utct_misc_configuration_feature][download][url] = "git@gitlab.com:utct/utct_misc_configuration_feature.git"
projects[utct_misc_configuration_feature][version] = "1.0"
projects[utct_misc_configuration_feature][download][tag] = "v1.2"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"
;++++++++  patch +++++++++
projects[profiler_builder][patch][] = "http://drupal.org/files/issues/profiler_builder-slashfix-2113331-6.patch"

projects[features][version] = "2.10"
projects[features][subdir] = "contrib"

projects[jquery_colorpicker][version] = "1.2"
projects[jquery_colorpicker][subdir] = "contrib"

projects[libraries][version] = "2.3"
projects[libraries][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[utct_default_site_template_pages_feature][type] = "module"
projects[utct_default_site_template_pages_feature][subdir] = "features"
projects[utct_default_site_template_pages_feature][download][type] = "git"
projects[utct_default_site_template_pages_feature][download][url] = "git@gitlab.com:utct/utct_default_site_template_pages_feature.git"
projects[utct_default_site_template_pages_feature][version] = "1.0"
projects[utct_default_site_template_pages_feature][download][tag] = "v1.3"

projects[panels][version] = "3.5"
projects[panels][subdir] = "contrib"

projects[panels_everywhere][version] = "1.0-rc2"
projects[panels_everywhere][subdir] = "contrib"

projects[shurly][version] = "1.x-dev"
projects[shurly][subdir] = "contrib"
projects[shurly][download][type] = "git"
projects[shurly][download][url] = "https://git.drupal.org/project/shurly.git"
projects[shurly][download][revision] = "6f279bc22ca4577c9fa6086d58e00e1b234a9e8f"
;++++++++ personal project patch +++++++++
projects[shurly][patch][] = "utct-patch-all.patch"

projects[utct_shurly_views_feature][type] = "module"
projects[utct_shurly_views_feature][subdir] = "features"
projects[utct_shurly_views_feature][download][type] = "git"
projects[utct_shurly_views_feature][download][url] = "git@gitlab.com:utct/utct_shurly_views_feature.git"
projects[utct_shurly_views_feature][version] = "1.0"
projects[utct_shurly_views_feature][download][tag] = "v1.3"

projects[views][version] = "3.14"
projects[views][subdir] = "contrib"

; +++++ Themes +++++

; omega
projects[omega][type] = "theme"
projects[omega][version] = "4.4"
projects[omega][subdir] = "contrib"

; +++++ Custom Sub Theme +++++
projects[utct][type] = theme
projects[utct][subdir] = contrib
projects[utct][download][type] = git
projects[utct][download][url] = git@gitlab.com:utct/utct_theme.git
projects[utct][download][tag] = "v1.2"
; +++++ End Custom Sub Theme +++++