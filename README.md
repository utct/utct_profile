# Site Installation Instructions

## Items in all caps prefaced by MY_ should be altered for your environment

## Change directory to local Drupal web server
    $ cd /

## Clone profile to local Drupal web server
    $ git clone git@gitlab.com:utct/utct_profile.git /MY_PROFILE_DIR

## Change directory to profile
    $ cd /MY_PROFILE_DIR

## Drush make the new site to your web root directory
    $ drush make local.make.example /MY_NEW_SITE_ROOT

## Change directory to /MY_NEW_SITE_ROOT
    $ cd MY_NEW_SITE_ROOT

## Copy default.settings.php file for Drupal install
    $ cp sites/default/default.settings.php sites/default/settings.php

## Change permissions for Drupal install
    $ chmod a+w sites/default/settings.php
    $ chmod a+w sites/default

## Create a new database with a user having all permissions, for example in MYSQL
    $ mysql -u MY_MYSQL_USER -p

## Enter your password at the prompt

## Create a database and assign it permissions
    $ create database MY_DRUPAL_DATABASE;
    $ GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER,
      CREATE TEMPORARY TABLES, LOCK TABLES ON MY_DRUPAL_DATABASE.* TO 'MY_MASTER_SITE_USER'@'localhost'
      IDENTIFIED BY 'MY_PASSWORD';
    $ exit;

# Navigate to your site and follow a typical Drupal install
